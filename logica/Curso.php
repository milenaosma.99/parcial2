<?php
require_once 'persistencia/CursoDAO.php';
require_once 'persistencia/Conexion.php';

class Curso  {
    private $idcurso;
    private $nombre;
    private $creditos;
    private $cursoDAO;
    private $conexion;
    
    
    
    
    /**
     * @return mixed
     */
    public function getIdcurso()
    {
        return $this->idcurso;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return mixed
     */
    public function getCreditos()
    {
        return $this->creditos;
    }

    function Curso ($idcurso="", $nombre="", $creditos=""){
  
        $this -> idcurso= $idcurso;
        $this -> nombre = $nombre;
        $this -> creditos = $creditos;
        $this -> conexion = new Conexion();
        $this -> cursoDAO = new CursoDAO($idcurso, $nombre, $creditos);
    }
    
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cursoDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> creditos = $resultado[1];
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cursoDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Curso($registro[0], $registro[1], $registro[2]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    function registrar(){
        echo $this -> cursoDAO -> registrar();
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> cursoDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    
}