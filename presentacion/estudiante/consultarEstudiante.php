<?php
require_once 'logica/Estudiante.php';

include 'presentacion/menu.php';
$estudiante = new Estudiante();
$estudiantes = $estudiante->consultarTodos();
// $profesor= new Profesor();
// $profesor->consultar();
?>
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header bg-secondary text-white">Consultar Estudiantes</div>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th scope="col">Id</th>
								<th scope="col">Nombre</th>
								<th scope="col">Apellido</th>
								<th scope="col">Servicios</th>
							</tr>
						</thead>
						<tbody>
						<?php
						foreach ($estudiantes as $e) {
        echo "<tr>";
        echo "<td>" . $e->getIdestudiante() . "</td>";
        echo "<td>" . $e->getNombre() . "</td>";
        echo "<td>" . $e->getApellido() . "</td>";
        
        echo "<td> <a class='fas fa-user-edit' href='index.php?pid=" . base64_encode("presentacion/curso/asignarCalificacion.php") . "&idEstudiante=" . $e->getIdEstudiante() . "' data-toggle='tooltip' data-placement='left' title='calificar'> </a>
             </td>";
        
        echo "<td>" . "<center><a class='fas fa-file-pdf' href='index.php?pid=" . base64_encode("presentacion/estudiante/verPDF.php") . "&idEstudiante=" . $e->getIdEstudiante() . "' data-toggle='tooltip' data-placement='left' title='pdf'> </a>" . "</td>";
        
        echo "</tr>";
    
    }
    echo "<tr><td colspan='11   '>" . count($estudiantes) . " registros encontrados</td></tr>"?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


