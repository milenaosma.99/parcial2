<?php
require_once 'persistencia/EstudianteDAO.php';
require_once 'persistencia/Conexion.php';

class Estudiante  {
    private $idestudiante;
    private $nombre;
    private $apellido;
    private $estudianteDAO;
    private $conexion;
    
    /**
     * @return mixed
     */
    public function getIdestudiante()
    {
        return $this->idestudiante;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @return mixed
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    function Estudiante ($idestudiante="", $nombre="", $apellido=""){
  
        $this -> idestudiante= $idestudiante;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> conexion = new Conexion();
        $this -> estudianteDAO = new EstudianteDAO($idestudiante, $nombre, $apellido);
    }
    
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Estudiante($registro[0], $registro[1], $registro[2]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO -> registrar());
        $this -> conexion -> cerrar();
    }
    
}