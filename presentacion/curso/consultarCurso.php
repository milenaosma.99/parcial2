<?php
require_once 'logica/Curso.php';
require_once 'logica/Estudiante.php';

include 'presentacion/menu.php';
$curso = new Curso();
$cursos = $curso->consultarTodos();

// $estudiante = new Estudiante();
// $estudiantes = $estudiante->consultarTodos();

?>
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header bg-secondary text-white">Consultar cursos</div>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th scope="col">Id</th>
								<th scope="col">nombre</th>
								<th scope="col">creditos</th>
<!-- 								<th scope="col">servicios</th> -->
		
								</tr>
						</thead>
						<tbody>
						<?php
						foreach ($cursos as $e) {
        echo "<tr>";
        echo "<td>" . $e->getIdcurso() . "</td>";
        echo "<td>" . $e->getNombre() . "</td>";
        echo "<td>" . $e->getCreditos() . "</td>";
        
//         echo "<td> <a class='fas fa-user-edit' href='index.php?pid=" . base64_encode("presentacion/curso/asignarCalificacion.php") . "&idcurso=" . $e->getIdestudiante() . "' data-toggle='tooltip' data-placement='left' title='calificar'> </a>
//              </td>";
        
//         echo "<td>" . "<center><a class='fas fa-file-pdf' href='index.php?pid=" . base64_encode("presentacion/estudiante/verPDF.php") . "&idestudiante=" . $e->getIdestudiante() . "' data-toggle='tooltip' data-placement='left' title='pdf'> </a>" . "</td>";
        
        
        echo "</tr>";
    
    }
    echo "<tr><td colspan='11   '>" . count($cursos) . " registros encontrados</td></tr>"?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


