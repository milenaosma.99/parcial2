<?php
require_once 'persistencia/Estudiante_cursoDAO.php';
require_once 'persistencia/Conexion.php';

class Estudiante_curso  {
    private $nota;
    private $estudiante_idestudiante;
    private $curso_idcurso;
    private $estudiante_cursoDAO;
    private $conexion;
    
    
    
    
    /**
     * @return mixed
     */
    public function getNota()
    {
        return $this->nota;
    }

    /**
     * @return mixed
     */
    public function getEstudiante_idestudiante()
    {
        return $this->estudiante_idestudiante;
    }

    /**
     * @return mixed
     */
    public function getCurso_idcurso()
    {
        return $this->curso_idcurso;
    }

    function Estudiante_curso ($nota="", $estudiante_idestudiante="", $curso_idcurso=""){
  
        $this -> nota= $nota;
        $this -> estudiante_idestudiante = $estudiante_idestudiante;
        $this -> curso_idcurso = $curso_idcurso;
        $this -> conexion = new Conexion();
        $this -> estudiante_cursoDAO = new Estudiante_cursoDAO($nota, $estudiante_idestudiante, $curso_idcurso);
    }
    
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudiante_cursoDAO-> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nota = $resultado[0];
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudiante_cursoDAO-> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Curso($registro[0], $registro[1], $registro[2]);
            $i++;
        }        
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    
}