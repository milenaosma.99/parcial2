<?php

if(isset($_POST['agregar'])){
    $nombre = $_POST['nombre'];
    $creditos = $_POST['creditos'];
    $curso = new Curso("", $nombre, $creditos);
        $curso -> registrar();
        $error=0;

}
include "presentacion/menu.php";

?>
<br></br>
<div class="row">
  <div class="col-s-12">
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-sm-4">
    </div>
    <div class="col-sm-4">
      <div class="card">
        <div class="card-body bg-secondary" >
          <h5 class="h5 text-dark  text-center">Crear curso</h5>
        </div>
      </div>
      <div class="card">
        <div class="card-body ">
<?php if (isset($_POST['agregar'])) { ?>
					<div class="alert alert-<?php echo ($error == 0) ? "success" : "danger" ?> alert-dismissible fade show" role="alert">
						<?php echo ($error == 0) ? "Registro de curso exitoso" : "El curso ya existe"; ?>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<?php } ?>


			<form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/curso/crearCurso.php") ?>">
			<div class="form-group">
              <input type="text" name="nombre" class="form-control"  required="required" placeholder="Nombre" >
            </div>
            <div class="form-group">
              <input type="text" name="creditos" class="form-control" required="required" placeholder="Creditos">
            </div>	
            <div class="text-center">
              <button type="submit" name="agregar" class="btn btn-secondary">Agregar</button>
            </div>
          </form>
        </div>
      </div>
      
    </div>
  </div>
</div>
